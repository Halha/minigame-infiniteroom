import component from './component';
import style from './style';
import { withStyles } from '@material-ui/core/styles';

export default withStyles(style)(component);
