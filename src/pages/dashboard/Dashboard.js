import React from 'react';
import Grid from '@material-ui/core/Grid';

import Main from '../../layouts/Main';
import Card from '../../components/fragment/Card';
import Carousel from '../../components/fragment/Coroussel';
import { dummy } from './constant';

const Component = ({ classes }) => {
  const TopSection = () => {
    return (
      <Grid
        container
        justify="center"
        spacing={3}
        style={{ marginBottom: '30px' }}
      >
        <Grid item sm={12} className={classes.section}>
          <h1>New Games!</h1>
          <Carousel data={dummy} />
        </Grid>
      </Grid>
    );
  };

  const GamesList = () => {
    return (
      <Grid item sm={8} className={classes.section}>
        <Grid container spacing={3}>
          <Grid item sm={12}>
            <h1>All Games</h1>
          </Grid>
          {renderCards()}
        </Grid>
      </Grid>
    );
  };

  const Friends = () => {
    return (
      <Grid item sm={3} className={classes.section}>
        <h1>Friends</h1>
        <p style={{ textAlign: 'center' }}>No one online</p>
      </Grid>
    );
  };

  const renderCards = () => {
    return dummy.map((data) => <Card key={`Game-${data.id}`} data={data} />);
  };

  return (
    <Main>
      <TopSection />
      <Grid
        container
        justify="space-between"
        alignItems="flex-start"
        spacing={3}
      >
        <GamesList />
        <Friends />
      </Grid>
    </Main>
  );
};

export default Component;
