import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import configureStore from './store/configureStore';
import { ROUTES } from './config';
import pages from './pages';

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path={ROUTES.dashboard} component={pages.dashboard} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
