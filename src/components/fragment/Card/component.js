import React from 'react';
import Grid from '@material-ui/core/Grid';

import Genre from '../../elements/Genre';
import Rate from '../../elements/Rate';

const Component = ({ data, classes }) => {
  const Genres = () => {
    return data.genre.map((genre, item) => <Genre key={item} genre={genre} />);
  };

  return (
    <Grid item sm={12}>
      <Grid container spacing={3} alignItems="center">
        <Grid item sm={3}>
          <img
            className={classes.img}
            src={require(`../../../${data.imageUrl}`)}
          />
        </Grid>
        <Grid item sm={9}>
          <h2>{data.title}</h2>
          <Grid container>
            <Grid item sm={7}>
              <div
                style={{
                  display: 'flex',
                  flexWrap: 'wrap',
                  marginBottom: '15px',
                }}
              >
                <Genres />
              </div>
              <Rate rating={data.rating} />
            </Grid>
            <Grid item sm={5}>
              <a>Test Button</a>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Component;
