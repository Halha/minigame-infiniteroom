import React from 'react';
import Carousel from 'react-material-ui-carousel';
import Paper from '@material-ui/core/Paper';

const component = ({ data, classes }) => {
  const Slide = ({ item }) => {
    return (
      <Paper
        className={classes.slide}
        style={{ padding: '0 70px', boxShadow: 'none' }}
      >
        <img
          style={{ width: '100%' }}
          src={require(`../../../${item.imageUrl}`)}
        />
      </Paper>
    );
  };

  return (
    <Carousel animation="slide" navButtonsAlwaysVisible>
      {data.map((item, index) => (
        <Slide key={index} item={item} />
      ))}
    </Carousel>
  );
};

export default component;
