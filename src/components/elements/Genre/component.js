import React from 'react';

const component = ({ genre, key, classes }) => {
  return (
    <div key={key} className={classes.button}>
      {genre}
    </div>
  );
};

export default component;
