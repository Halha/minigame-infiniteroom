import React from 'react';
import clsx from 'clsx';
import Achievement from '../../../assets/svg/Achievement';
import Dashboard from '../../../assets/svg/Dashboard';
import Friends from '../../../assets/svg/Friends';
import LeaderBoard from '../../../assets/svg/LeaderBoard';
import Logout from '../../../assets/svg/Logout';
import MyGames from '../../../assets/svg/MyGames';
import Setting from '../../../assets/svg/Setting';
import History from '../../../assets/svg/History';

const component = ({ classes, active, type }) => {
  const iconProps = {
    className: clsx(classes.icon, { [classes.iconActive]: active }),
  };

  switch (type) {
    case 'achievement': {
      return <Achievement {...iconProps} />;
    }
    case 'dashboard': {
      return <Dashboard {...iconProps} />;
    }
    case 'friends': {
      return <Friends {...iconProps} />;
    }
    case 'leaderboard': {
      return <LeaderBoard {...iconProps} />;
    }
    case 'logout': {
      return <Logout {...iconProps} />;
    }
    case 'mygames': {
      return <MyGames {...iconProps} />;
    }
    case 'setting': {
      return <Setting {...iconProps} />;
    }
    case 'history': {
      return <History {...iconProps} />;
    }
    default: {
      return null;
    }
  }
};

export default component;
