const styles = {
  icon: {
    color: '#FF5516',
    height: '20px',
    width: '20px',
  },
  iconActive: {
    color: '#FFBA00 !important',
  },
};

export default styles;
